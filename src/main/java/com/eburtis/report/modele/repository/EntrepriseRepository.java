package com.eburtis.report.modele.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eburtis.report.modele.entite.Entreprise;

public interface EntrepriseRepository extends JpaRepository<Entreprise, Long>, CrudRepository {

	Entreprise findByCode(String code);
}
