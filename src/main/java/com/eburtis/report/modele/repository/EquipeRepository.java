package com.eburtis.report.modele.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eburtis.report.modele.entite.Equipe;

public interface EquipeRepository extends JpaRepository<Equipe, Long>, CrudRepository {

	Equipe findByCode(String code);
}
