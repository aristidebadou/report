package com.eburtis.report.modele.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eburtis.report.modele.entite.LigneCra;

public interface LigneCraRepository extends JpaRepository<LigneCra, Long>, CrudRepository {

}
