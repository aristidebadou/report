package com.eburtis.report.modele.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eburtis.report.modele.entite.SousTache;

public interface SousTacheRepository extends JpaRepository<SousTache, Long>, CrudRepository {

}
