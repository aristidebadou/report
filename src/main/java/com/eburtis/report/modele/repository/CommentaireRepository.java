package com.eburtis.report.modele.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eburtis.report.modele.entite.Commentaire;

public interface CommentaireRepository extends JpaRepository<Commentaire, Long>, CrudRepository {

}
