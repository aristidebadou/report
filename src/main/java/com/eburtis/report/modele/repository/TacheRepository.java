package com.eburtis.report.modele.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eburtis.report.modele.entite.Tache;

public interface TacheRepository extends JpaRepository<Tache, Long>, CrudRepository {

}
