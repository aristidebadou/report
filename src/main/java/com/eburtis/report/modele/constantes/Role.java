package com.eburtis.report.modele.constantes;

public enum Role {
	DEVELOPPEUR("Développeur"),
	TESTEUR("Testeur(se)");

	private String libelle;

	Role(String libelle) {
		this.libelle = libelle;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
}
