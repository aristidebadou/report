package com.eburtis.report.modele.entite;

import static com.eburtis.report.modele.constantes.JpaConstants.ID;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = Commentaire.TABLE_NAME)
public class Commentaire extends AbstractEntity {

	public static final String TABLE_NAME = "commentaire";
	public static final String TABLE_ID = TABLE_NAME + ID;

	@Id
	@GeneratedValue
	private Long id;
	private LocalDateTime dateHeure;
	private String contenu;

	@JoinColumn(name = Utilisateur.TABLE_ID, nullable = false)
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Utilisateur utilisateur;

	@JoinColumn(name = Cra.TABLE_ID, nullable = false)
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Cra cra;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDateHeure() {
		return dateHeure;
	}

	public void setDateHeure(LocalDateTime dateHeure) {
		this.dateHeure = dateHeure;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Cra getCra() {
		return cra;
	}

	public void setCra(Cra cra) {
		this.cra = cra;
	}
}
