package com.eburtis.report.modele.entite;

import static com.eburtis.report.modele.constantes.JpaConstants.ID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = Equipe.TABLE_NAME)
public class Equipe extends AbstractEntity {

	public static final String TABLE_NAME = "equipe";
	public static final String TABLE_ID = TABLE_NAME + ID;

	@Id
	@GeneratedValue
	private Long id;
	private String code;
	private String designation;
	private String description;

	public Equipe() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
