package com.eburtis.report.modele.entite;

import static com.eburtis.report.modele.constantes.JpaConstants.ID;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

import com.eburtis.report.modele.constantes.StatutEnvoi;

@Entity
@Table(name = Cra.TABLE_NAME)
public class Cra extends AbstractEntity {

	public static final String TABLE_NAME = "cra";
	public static final String TABLE_ID = TABLE_NAME + ID;

	@Id
	@GeneratedValue
	private Long id;
	private LocalDate date;
	private String contenu;
	private StatutEnvoi statutEnvoi;
	private LocalDateTime dateEnvoi;
	@JoinColumn(name = Utilisateur.TABLE_ID, nullable = false)
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Utilisateur utilisateur;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public StatutEnvoi getStatutEnvoi() {
		return statutEnvoi;
	}

	public void setStatutEnvoi(StatutEnvoi statutEnvoi) {
		this.statutEnvoi = statutEnvoi;
	}

	public LocalDateTime getDateEnvoi() {
		return dateEnvoi;
	}

	public void setDateEnvoi(LocalDateTime dateEnvoi) {
		this.dateEnvoi = dateEnvoi;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
}
