package com.eburtis.report.modele.entite;

import static com.eburtis.report.modele.constantes.JpaConstants.ID;
import static javax.persistence.FetchType.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = SousTache.TABLE_NAME)
public class SousTache extends AbstractEntity {

	public static final String TABLE_NAME = "sous_tache";
	public static final String TABLE_ID = TABLE_NAME + ID;

	@Id
	@GeneratedValue
	private Long id;
	private String code;
	private String designation;
	private String description;

	@JoinColumn(name = Tache.TABLE_ID, nullable = false)
	@ManyToOne(fetch = LAZY, optional = false)
	private Tache tache;

	@JoinColumn(name = LigneCra.TABLE_ID, nullable = false)
	@OneToOne(fetch = LAZY, optional = false)
	private LigneCra ligneCra;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Tache getTache() {
		return tache;
	}

	public void setTache(Tache tache) {
		this.tache = tache;
	}

	public LigneCra getLigneCra() {
		return ligneCra;
	}

	public void setLigneCra(LigneCra ligneCra) {
		this.ligneCra = ligneCra;
	}
}
