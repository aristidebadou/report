package com.eburtis.report.modele.entite;

import static com.eburtis.report.modele.constantes.JpaConstants.ID;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = Tache.TABLE_NAME)
public class Tache extends AbstractEntity {

	public static final String TABLE_NAME = "tache";
	public static final String TABLE_ID = TABLE_NAME + ID;

	@Id
	@GeneratedValue
	private Long id;
	private String code;
	private String designation;
	private String description;
	private Integer complexite;

	@OneToMany(fetch = LAZY, cascade = ALL, mappedBy = "tache", orphanRemoval = true)
	private List<SousTache> sousTaches;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getComplexite() {
		return complexite;
	}

	public void setComplexite(Integer complexite) {
		this.complexite = complexite;
	}

	public List<SousTache> getSousTaches() {
		return sousTaches;
	}

	public void setSousTaches(List<SousTache> sousTaches) {
		this.sousTaches = sousTaches;
	}
}
