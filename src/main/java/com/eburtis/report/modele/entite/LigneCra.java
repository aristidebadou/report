package com.eburtis.report.modele.entite;

import static com.eburtis.report.modele.constantes.JpaConstants.ID;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = LigneCra.TABLE_NAME)
public class LigneCra extends AbstractEntity {

	public static final String TABLE_NAME = "ligne_cra";
	public static final String TABLE_ID = TABLE_NAME + ID;

	@Id
	@GeneratedValue
	private Long id;
	private String commentaire;
	@JoinColumn(name = Cra.TABLE_ID, nullable = false)
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Cra cra;

	@OneToOne(fetch = LAZY, cascade = ALL, mappedBy = "ligneCra", orphanRemoval = true)
	private SousTache sousTache;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Cra getCra() {
		return cra;
	}

	public void setCra(Cra cra) {
		this.cra = cra;
	}

	public SousTache getSousTache() {
		return sousTache;
	}

	public void setSousTache(SousTache sousTache) {
		this.sousTache = sousTache;
	}
}
