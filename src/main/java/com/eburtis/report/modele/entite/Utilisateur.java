package com.eburtis.report.modele.entite;

import static com.eburtis.report.modele.constantes.JpaConstants.ID;
import static com.eburtis.report.modele.constantes.JpaConstants.SEQ;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.LAZY;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.eburtis.report.modele.constantes.Role;

@Entity
@Table(name = Utilisateur.TABLE_NAME)
public class Utilisateur extends AbstractEntity {

	public static final String TABLE_NAME = "utilisateur";
	public static final String TABLE_ID = TABLE_NAME + ID;
	private static final String TABLE_SEQ = TABLE_ID + SEQ;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
	@SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
	private Long id;

	private String code;

	private String nom;

	private String prenoms;

	private String genre;

	private String contact;

	private String email;

	@Enumerated(STRING)
	private Role role;

	private String login;

	private String password;

	@JoinColumn(name = Entreprise.TABLE_ID, nullable = false)

	@ManyToOne(fetch = LAZY, optional = false)
	private Entreprise entreprise;

	@JoinColumn(name = Equipe.TABLE_ID, nullable = false)
	@ManyToOne(fetch = LAZY, optional = false)
	private Equipe equipe;

	public Utilisateur() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenoms() {
		return prenoms;
	}

	public void setPrenoms(String prenoms) {
		this.prenoms = prenoms;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Entreprise getEntreprise() {
		return entreprise;
	}

	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}

	public Equipe getEquipe() {
		return equipe;
	}

	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}
}
