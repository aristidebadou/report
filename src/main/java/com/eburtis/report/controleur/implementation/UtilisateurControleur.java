package com.eburtis.report.controleur.implementation;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eburtis.report.controleur.CrudControleur;
import com.eburtis.report.controleur.dto.UtilisateurDto;
import com.eburtis.report.controleur.mapper.UtilisateurMapper;
import com.eburtis.report.modele.entite.Utilisateur;
import com.eburtis.report.service.implementation.UtilisateurService;

@RestController
@RequestMapping(value = "/ws/utilisateur")
public class UtilisateurControleur implements CrudControleur<UtilisateurDto> {

	private final UtilisateurMapper utilisateurMapper;
	private final UtilisateurService utilisateurService;

	public UtilisateurControleur(UtilisateurMapper utilisateurMapper, UtilisateurService utilisateurService) {
		this.utilisateurMapper = utilisateurMapper;
		this.utilisateurService = utilisateurService;
	}

	@Override
	@PostMapping
	public void creerOuModifier(@RequestBody UtilisateurDto dto) {
		Utilisateur utilisateur = utilisateurMapper.mapDtoToModel(dto);
		utilisateurService.creerOuModifier(utilisateur);
	}

	@Override
	public void creerOuModifierTous(Collection<UtilisateurDto> dtos) {

	}

	@Override
	@DeleteMapping
	public void supprimer(@RequestParam(value = "id") long id) {
		utilisateurService.supprimer(id);
	}

	@Override
	public void supprimerTous(Collection<UtilisateurDto> dtos) {

	}

	@Override
	@GetMapping
	public List<UtilisateurDto> lister() {
		return utilisateurMapper.mapModelsToDtos(utilisateurService.lister());
	}

	@Override
	public Set<UtilisateurDto> listerSansDoublon() {
		return null;
	}

	@Override
	public Map<Long, UtilisateurDto> listerParId() {
		return null;
	}

	@PutMapping(value = "/generer")
	public void genererCode(@RequestBody UtilisateurDto dto) {
		Utilisateur utilisateur = utilisateurMapper.mapDtoToModel(dto);
		utilisateurService.genererCodeUtilisateur(utilisateur);
	}
}
