package com.eburtis.report.controleur.implementation;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eburtis.report.controleur.CrudControleur;
import com.eburtis.report.controleur.dto.EntrepriseDto;
import com.eburtis.report.controleur.mapper.EntrepriseMapper;
import com.eburtis.report.service.implementation.EntrepriseService;

@RestController
@RequestMapping(value = {"/ws/entreprise"})
public class EntrepriseControleur implements CrudControleur<EntrepriseDto> {

	private final EntrepriseMapper entrepriseMapper;
	private final EntrepriseService entrepriseService;

	public EntrepriseControleur(EntrepriseMapper entrepriseMapper, EntrepriseService entrepriseService) {
		this.entrepriseMapper = entrepriseMapper;
		this.entrepriseService = entrepriseService;
	}

	@Override
	public void creerOuModifier(EntrepriseDto dto) {

	}

	@Override
	public void creerOuModifierTous(Collection<EntrepriseDto> dtos) {

	}

	@Override
	public void supprimer(long id) {

	}

	@Override
	public void supprimerTous(Collection<EntrepriseDto> dtos) {

	}

	@Override
	@GetMapping
	public List<EntrepriseDto> lister() {
		return entrepriseMapper.mapModelsToDtos(entrepriseService.lister());
	}

	@Override
	public Set<EntrepriseDto> listerSansDoublon() {
		return null;
	}

	@Override
	public Map<Long, EntrepriseDto> listerParId() {
		return null;
	}
}
