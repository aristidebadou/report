package com.eburtis.report.controleur.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import com.eburtis.report.controleur.dto.UtilisateurDto;
import com.eburtis.report.controleur.mapper.abstraction.IMapper;
import com.eburtis.report.modele.constantes.Role;
import com.eburtis.report.modele.entite.Utilisateur;

/**
 * Mapper UtilisateurDto/Utilisateur
 */
@Mapper(uses = {EntrepriseMapper.class, EquipeMapper.class})
public interface UtilisateurMapper extends IMapper<UtilisateurDto, Utilisateur> {

	@Override
	@Mapping(target = "id", source = "id")
	@Mapping(target = "login", ignore = true)
	@Mapping(target = "password", ignore = true)
	@Mapping(target = "role", source = "role", qualifiedByName = "enumRoleLibelle")
	UtilisateurDto mapToDto(Utilisateur utilisateur);

	@Override
	@Mapping(target = "role", source = "role", qualifiedByName = "enumLibelleToRole")
	Utilisateur mapDtoToModel(UtilisateurDto dto);

	@Named("enumRoleLibelle")
	default String roleLibelle(final Role role) {
		return role.getLibelle();
	}

	@Named("enumLibelleToRole")
	default Role libelleToRole(final String libelle) {
		return Role.valueOf(libelle.toUpperCase());
	}
}
