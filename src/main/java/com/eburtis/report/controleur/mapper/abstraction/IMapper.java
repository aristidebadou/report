package com.eburtis.report.controleur.mapper.abstraction;

import com.eburtis.report.controleur.dto.AbstractDto;
import com.eburtis.report.modele.entite.AbstractEntity;

/**
 * Interface Mapper.
 *
 * @param <DTO> paramétre générique DTO.
 * @param <MODEL> paramétre générique MODEL.
 */
public interface IMapper<DTO extends AbstractDto, MODEL extends AbstractEntity> extends IMapperDtoModel<DTO, MODEL> {

}

