package com.eburtis.report.controleur.mapper;

import org.mapstruct.Mapper;

import com.eburtis.report.controleur.dto.SousTacheDto;
import com.eburtis.report.controleur.mapper.abstraction.IMapper;
import com.eburtis.report.modele.entite.SousTache;

/**
 * Mapper SousTacheDto/SousTache
 */
@Mapper
public interface SousTacheMapper extends IMapper<SousTacheDto, SousTache> {

}
