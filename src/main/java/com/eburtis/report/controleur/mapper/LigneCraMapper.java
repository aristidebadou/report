package com.eburtis.report.controleur.mapper;

import org.mapstruct.Mapper;

import com.eburtis.report.controleur.dto.LigneCraDto;
import com.eburtis.report.controleur.mapper.abstraction.IMapper;
import com.eburtis.report.modele.entite.LigneCra;

/**
 * Mapper LigneCraDto/LigneCra
 */
@Mapper
public interface LigneCraMapper extends IMapper<LigneCraDto, LigneCra> {

}
