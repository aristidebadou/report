package com.eburtis.report.controleur.mapper.abstraction;

import java.util.List;

import org.mapstruct.MapperConfig;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import com.eburtis.report.controleur.dto.AbstractDto;
import com.eburtis.report.modele.entite.AbstractEntity;

@MapperConfig
public interface IMapperDtoModel<DTO extends AbstractDto, MODEL extends AbstractEntity> {

	/**
	 * Map DTO to MODEL.
	 *
	 * @param dto DTO.
	 * @return MODEL.
	 */
	MODEL mapDtoToModel(DTO dto);

	/**
	 * Map MODEL to DTO.
	 *
	 * @param model MODEL.
	 * @return DTO.
	 */
	@Mapping(target = "id", ignore = true)
	DTO mapToDto(MODEL model);

	/**
	 * Map list MODEL to DTO.
	 *
	 * @param models list model.
	 * @return list vo.
	 */
	@Mapping(target = "id", ignore = true)
	List<DTO> mapModelsToDtos(List<MODEL> models);

	/**
	 * Map list DTO to MODEL.
	 *
	 * @param dtos list DTO.
	 * @return list MODEL.
	 */
	List<MODEL> mapDtoToModel(List<DTO> dtos);

	/**
	 * Merge dto in model.
	 * @param modelTarget MODEL.
	 * @param dtoSource DTO.
	 * @return MODEL.
	 */
	@Mapping(target = "id", ignore = true)
	MODEL mergeDtoInModel(@MappingTarget MODEL modelTarget, DTO dtoSource);

	/**
	 * Merge modelSource in modelTarget.
	 * @param modelTarget MODEL.
	 * @param modelSource MODEL.
	 * @return MODEL.
	 */
	@Mapping(target = "id", ignore = true)
	MODEL mergeModel(@MappingTarget MODEL modelTarget, MODEL modelSource);
}
