package com.eburtis.report.controleur;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface CrudControleur<T> {

	/**
	 * Crée ou modifie l'entité.
	 *
	 * @param dto l'entité.
	 */
	void creerOuModifier(T dto);

	/**
	 * Crée ou modifie la liste des entités.
	 *
	 * @param dtos la liste des entités.
	 */
	void creerOuModifierTous(Collection<T> dtos);

	/**
	 * Supprime l'entité.
	 *
	 * @param id l'identifiant de l'entité à supprimer.
	 */
	void supprimer(long id);

	/**
	 * Supprime la liste des entités.
	 *
	 * @param dtos la liste des entités.
	 */
	void supprimerTous(Collection<T> dtos);

	/**
	 * Retourne la liste des entités.
	 *
	 * @return la liste des entités
	 */
	List<T> lister();

	/**
	 * Retourne la liste des entités sans doublon.
	 *
	 * @return la liste des entités.
	 */
	Set<T> listerSansDoublon();

	/**
	 * Retourne la liste des entités indexées par leur identifiant.
	 *
	 * @return la liste des entités
	 */
	Map<Long, T> listerParId();
}
