package com.eburtis.report.controleur.dto;

public class SousTacheDto extends AbstractDto {

	private Long id;
	private String code;
	private String designation;
	private String description;
	private TacheDto tache;
	private LigneCraDto ligneCra;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public TacheDto getTache() {
		return tache;
	}

	public void setTache(TacheDto tache) {
		this.tache = tache;
	}

	public LigneCraDto getLigneCra() {
		return ligneCra;
	}

	public void setLigneCra(LigneCraDto ligneCra) {
		this.ligneCra = ligneCra;
	}
}
