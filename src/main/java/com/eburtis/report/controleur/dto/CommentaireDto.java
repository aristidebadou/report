package com.eburtis.report.controleur.dto;

import java.time.LocalDateTime;

public class CommentaireDto extends AbstractDto {

	private Long id;
	private LocalDateTime dateHeure;
	private String contenu;
	private UtilisateurDto utilisateur;
	private CraDto cra;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDateHeure() {
		return dateHeure;
	}

	public void setDateHeure(LocalDateTime dateHeure) {
		this.dateHeure = dateHeure;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public UtilisateurDto getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurDto utilisateur) {
		this.utilisateur = utilisateur;
	}

	public CraDto getCra() {
		return cra;
	}

	public void setCra(CraDto cra) {
		this.cra = cra;
	}
}
