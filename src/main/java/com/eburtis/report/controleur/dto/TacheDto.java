package com.eburtis.report.controleur.dto;

import java.util.List;

public class TacheDto extends AbstractDto {

	private Long id;
	private String code;
	private String designation;
	private String description;
	private Integer complexite;
	private List<SousTacheDto> sousTaches;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getComplexite() {
		return complexite;
	}

	public void setComplexite(Integer complexite) {
		this.complexite = complexite;
	}

	public List<SousTacheDto> getSousTaches() {
		return sousTaches;
	}

	public void setSousTaches(List<SousTacheDto> sousTaches) {
		this.sousTaches = sousTaches;
	}
}
