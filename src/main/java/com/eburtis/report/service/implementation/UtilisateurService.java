package com.eburtis.report.service.implementation;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.*;
import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.eburtis.report.controleur.mapper.UtilisateurMapper;
import com.eburtis.report.modele.entite.Entreprise;
import com.eburtis.report.modele.entite.Equipe;
import com.eburtis.report.modele.entite.Utilisateur;
import com.eburtis.report.modele.repository.EntrepriseRepository;
import com.eburtis.report.modele.repository.EquipeRepository;
import com.eburtis.report.modele.repository.UtilisateurRepository;
import com.eburtis.report.service.CrudService;

@Service
public class UtilisateurService implements CrudService<Utilisateur> {

	private final UtilisateurMapper utilisateurMapper;
	private final EntrepriseRepository entrepriseRepository;
	private final EquipeRepository equipeRepository;
	private final UtilisateurRepository utilisateurRepository;

	public UtilisateurService(UtilisateurMapper utilisateurMapper, EntrepriseRepository entrepriseRepository, EquipeRepository equipeRepository,
							  UtilisateurRepository utilisateurRepository) {
		this.utilisateurMapper = utilisateurMapper;
		this.entrepriseRepository = entrepriseRepository;
		this.equipeRepository = equipeRepository;
		this.utilisateurRepository = utilisateurRepository;
	}

	@Override
	public void creerOuModifier(final Utilisateur utilisateur) {
		Equipe equipe = equipeRepository.findByCode(utilisateur.getEquipe().getCode());
		Entreprise entreprise = entrepriseRepository.findByCode(utilisateur.getEntreprise().getCode());
		Utilisateur utilisateurToSave = nonNull(utilisateur.getId()) ? utilisateurRepository.findById(utilisateur.getId()).orElse(utilisateur) : utilisateur;
		if (nonNull(utilisateurToSave.getId())) {
			utilisateurMapper.mergeModel(utilisateurToSave, utilisateur);
		}
		else {
			utilisateurToSave.setEntreprise(entreprise);
			utilisateurToSave.setEquipe(equipe);
		}
		utilisateurRepository.save(utilisateurToSave);
	}

	@Override
	public void creerOuModifierTous(Collection<Utilisateur> entities) {

	}

	@Override
	public void supprimer(long id) {
		Utilisateur utilisateurToDelete = utilisateurRepository.findById(id).orElse(null);
		if (nonNull(utilisateurToDelete)) {
			utilisateurRepository.delete(utilisateurToDelete);
		}
	}

	@Override
	public void supprimerTous(Collection<Utilisateur> entities) {

	}

	@Override
	public List<Utilisateur> lister() {
		return utilisateurRepository.findAll();
	}

	@Override
	public Set<Utilisateur> listerSansDoublon() {
		return null;
	}

	@Override
	public Map<Long, Utilisateur> listerParId() {
		return null;
	}

	public void genererCodeUtilisateur(Utilisateur utilisateur) {
		String code = utilisateur.getPrenoms().substring(0, 1).toUpperCase() + utilisateur.getNom().substring(0, 2).toUpperCase();
		List<String> codeExistants = utilisateurRepository.findByCodeLike(code + "%").stream()
				.map(Utilisateur::getCode)
				.collect(toList());

		utilisateur.setCode(isNotEmpty(codeExistants) ? code + (codeExistants.size() + 1) : code);

		Utilisateur utilisateurToSave = utilisateurRepository.findById(utilisateur.getId()).orElse(utilisateur);
		if (nonNull(utilisateurToSave.getId())) {
			utilisateurMapper.mergeModel(utilisateurToSave, utilisateur);
			utilisateurRepository.save(utilisateurToSave);
		}
	}
}
