package com.eburtis.report.service.implementation;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.eburtis.report.modele.entite.Entreprise;
import com.eburtis.report.modele.repository.EntrepriseRepository;
import com.eburtis.report.service.CrudService;

@Service
public class EntrepriseService implements CrudService<Entreprise> {

	private final EntrepriseRepository entrepriseRepository;
	public EntrepriseService(EntrepriseRepository entrepriseRepository) {
		this.entrepriseRepository = entrepriseRepository;
	}

	@Override
	public void creerOuModifier(Entreprise entity) {

	}

	@Override
	public void creerOuModifierTous(Collection<Entreprise> entities) {

	}

	@Override
	public void supprimer(long id) {

	}

	@Override
	public void supprimerTous(Collection<Entreprise> entities) {

	}

	@Override
	public List<Entreprise> lister() {
		return entrepriseRepository.findAll();
	}

	@Override
	public Set<Entreprise> listerSansDoublon() {
		return null;
	}

	@Override
	public Map<Long, Entreprise> listerParId() {
		return null;
	}
}
