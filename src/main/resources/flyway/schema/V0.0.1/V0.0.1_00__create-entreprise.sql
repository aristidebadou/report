-- public.entreprise definition

-- Drop table

-- DROP TABLE public.entreprise;

CREATE TABLE public.entreprise (
	id int8 NOT NULL,
	adresse varchar(255) NULL,
	code varchar(255) NULL,
	contact varchar(255) NULL,
	email varchar(255) NULL,
	pays varchar(255) NULL,
	raison_sociale varchar(255) NULL,
	"type" varchar(255) NULL,
	ville varchar(255) NULL,
	CONSTRAINT entreprise_pkey PRIMARY KEY (id)
);
