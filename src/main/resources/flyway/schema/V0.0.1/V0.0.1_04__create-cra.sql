-- public.cra definition

-- Drop table

-- DROP TABLE public.cra;

CREATE TABLE public.cra (
	id int8 NOT NULL,
	contenu varchar(255) NULL,
	"date" date NULL,
	date_envoi timestamp NULL,
	statut_envoi int4 NULL,
	utilisateur_id int8 NOT NULL,
	CONSTRAINT cra_pkey PRIMARY KEY (id)
);


-- public.cra foreign keys

ALTER TABLE public.cra ADD CONSTRAINT fk5qt0eaixwq97cawd51uy7c86 FOREIGN KEY (utilisateur_id) REFERENCES public.utilisateur(id);
