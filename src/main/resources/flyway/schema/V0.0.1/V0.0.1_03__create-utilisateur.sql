-- public.utilisateur definition

-- Drop table

-- DROP TABLE public.utilisateur;
CREATE SEQUENCE utilisateur_id_seq START WITH 1 INCREMENT BY 50;
CREATE TABLE public.utilisateur (
	id int8 NOT NULL,
	code varchar(255) NULL,
	contact varchar(255) NULL,
	email varchar(255) NULL,
	genre varchar(255) NULL,
	login varchar(255) NULL,
	nom varchar(255) NULL,
	"password" varchar(255) NULL,
	prenoms varchar(255) NULL,
	"role" varchar(255) NULL,
	entreprise_id int8 NOT NULL,
	equipe_id int8 NOT NULL,
	CONSTRAINT utilisateur_pkey PRIMARY KEY (id)
);


-- public.utilisateur foreign keys
ALTER TABLE utilisateur ALTER COLUMN id SET DEFAULT nextval('utilisateur_id_seq');
ALTER TABLE public.utilisateur ADD CONSTRAINT fk8fjtucbyo2t6agaejym2j764f FOREIGN KEY (entreprise_id) REFERENCES public.entreprise(id);
ALTER TABLE public.utilisateur ADD CONSTRAINT fkl8kxatycepq0poeoqjxhjwuik FOREIGN KEY (equipe_id) REFERENCES public.equipe(id);
