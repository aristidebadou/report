-- public.ligne_cra definition

-- Drop table

-- DROP TABLE public.ligne_cra;

CREATE TABLE public.ligne_cra (
	id int8 NOT NULL,
	commentaire varchar(255) NULL,
	cra_id int8 NOT NULL,
	CONSTRAINT ligne_cra_pkey PRIMARY KEY (id)
);


-- public.ligne_cra foreign keys

ALTER TABLE public.ligne_cra ADD CONSTRAINT fkfi4gla3eakhj0yvl0c3oel6p5 FOREIGN KEY (cra_id) REFERENCES public.cra(id);
