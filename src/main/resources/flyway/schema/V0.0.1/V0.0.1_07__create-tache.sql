-- public.tache definition

-- Drop table

-- DROP TABLE public.tache;

CREATE TABLE public.tache (
	id int8 NOT NULL,
	code varchar(255) NULL,
	complexite int4 NULL,
	description varchar(255) NULL,
	designation varchar(255) NULL,
	CONSTRAINT tache_pkey PRIMARY KEY (id)
);
