-- public.equipe definition

-- Drop table

-- DROP TABLE public.equipe;

CREATE TABLE public.equipe (
	id int8 NOT NULL,
	code varchar(255) NULL,
	description varchar(255) NULL,
	designation varchar(255) NULL,
	CONSTRAINT equipe_pkey PRIMARY KEY (id)
);
