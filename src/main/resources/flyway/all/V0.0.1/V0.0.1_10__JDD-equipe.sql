INSERT INTO public.equipe
(id, code, designation, description)
VALUES
(1, 'EQR', 'Equipe Remote', 'Equipe mixte travaillant sur les applications Sigale Qualité et C-KDO'),
(2, 'ECI', 'Equipe Côte d''Ivoire', 'Equipe à Abidjan, travaillant sur les applications Sigale Achats, Pricing, Facturation et Dons');
