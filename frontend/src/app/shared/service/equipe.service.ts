import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { urls } from './urls';
import { Injectable } from '@angular/core';
import { Entreprise } from '../model/entreprise.model';
import { Equipe } from '../model/equipe.model';

@Injectable({
	providedIn: 'root'
})
export class EquipeService {

	constructor(private http: HttpClient) {
	}

	/**
	 * Liste toutes les équipes.
	 */
	lister(): Observable<Equipe[]> {
		return this.http.get<Equipe[]>(urls.equipe);
	}
}
