import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { urls } from './urls';
import { Injectable } from '@angular/core';
import { Entreprise } from '../model/entreprise.model';

@Injectable({
	providedIn: 'root'
})
export class EntrepriseService {

	constructor(private http: HttpClient) {
	}

	/**
	 * Liste toutes les entrepises.
	 */
	lister(): Observable<Entreprise[]> {
		return this.http.get<Entreprise[]>(urls.entreprise);
	}
}
