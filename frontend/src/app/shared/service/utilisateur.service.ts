import { HttpClient, HttpParams } from '@angular/common/http';
import { Utilisateur } from '../model/utilisateur.model';
import { Observable } from 'rxjs';
import { urls } from './urls';
import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class UtilisateurService {

	constructor(private http: HttpClient) {
	}

	/**
	 * Lister les articles en fonction des rayons autorisés.
	 */
	lister(): Observable<Utilisateur[]> {
		return this.http.get<Utilisateur[]>(urls.utilisateur);
	}

	/**
	 * Crée ou modifie un nouvel utilisateur.
	 */
	creer(utilisateur: Utilisateur | undefined): Observable<void> {
		return this.http.post<void>(urls.utilisateur, utilisateur);
	}

	/**
	 * Suppimer un utilisateur.
	 */
	supprimer(id: number): Observable<void> {
		const params = new HttpParams().append('id', id!.toString());
		return this.http.delete<void>(urls.utilisateur, {params: params});
	}

	/**
	 * Génère un code pour l'utilisateur.
	 */
	genererCode(utilisateur: Utilisateur | undefined): Observable<void> {
		return this.http.put<void>(urls.utilisateur + '/generer', utilisateur);
	}
}
