export class Equipe {
	id?: number;
	code?: string;
	designation?: string;
	description?: string;

	constructor(id?: number, code?: string, designation?: string, description?: string) {
		this.id = id;
		this.code = code;
		this.designation = designation;
		this.description = description;
	}
}
