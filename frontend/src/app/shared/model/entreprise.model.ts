export class Entreprise {
	id?: number;
	code?: string;
	raisonSociale?: string;
	type?: string;
	contact?: string;
	email?: string;
	pays?: string;
	ville?: string;


	constructor(id?: number, code?: string, raisonSociale?: string, type?: string, contact?: string, email?: string, pays?: string, ville?: string) {
		this.id = id;
		this.code = code;
		this.raisonSociale = raisonSociale;
		this.type = type;
		this.contact = contact;
		this.email = email;
		this.pays = pays;
		this.ville = ville;
	}
}
