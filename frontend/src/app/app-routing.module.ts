import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './modules/accueil/accueil.component';

const routes: Routes = [
	{
		path: '',
		children: [
			{
				path: 'accueil',
				component: AccueilComponent
			},
			{
				path: 'utilisateur',
				loadChildren: () => import('./modules/organisation/organisation.module').then(m => m.OrganisationModule)
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
