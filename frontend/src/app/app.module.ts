import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccueilComponent } from './modules/accueil/accueil.component';
import { MenubarModule } from 'primeng/menubar';
import { SharedModule } from 'primeng/api';
import { UtilisateurModule } from './modules/organisation/utilisateur/utilisateur.module';
import { HttpClientModule } from '@angular/common/http';
import { OrganisationComponent } from './modules/organisation/organisation.component';
import { TabViewModule } from 'primeng/tabview';
import { OrganisationModule } from './modules/organisation/organisation.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

@NgModule({
	declarations: [
		AppComponent,
		AccueilComponent,
  		OrganisationComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		MenubarModule,
		SharedModule,
		UtilisateurModule,
		HttpClientModule,
		TabViewModule,
		OrganisationModule,
		BrowserAnimationsModule,
		ConfirmDialogModule,


	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
