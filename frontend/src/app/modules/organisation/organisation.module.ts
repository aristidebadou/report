import { NgModule } from '@angular/core';
import { OrganisationRoutingModule } from './organisation-routing.module';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { UtilisateurComponent } from './utilisateur/utilisateur.component';
import { UtilisateurModule } from './utilisateur/utilisateur.module';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';

@NgModule({
	declarations: [
		UtilisateurComponent
	],
	imports: [
		OrganisationRoutingModule,
		TableModule,
		InputTextModule,
		CommonModule,
		ButtonModule,
		UtilisateurModule,
		ConfirmDialogModule,
	],
	providers: [ConfirmationService],
	exports: [
		UtilisateurComponent
	],
	bootstrap: []
})
export class OrganisationModule {
}
