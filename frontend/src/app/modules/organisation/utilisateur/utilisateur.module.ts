import { NgModule } from '@angular/core';
import { UtilisateurRoutingModule } from './utilisateur-routing.module';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { UtilisateurModalComponent } from './utilisateur-modal/utilisateur-modal.component';
import { DialogModule } from 'primeng/dialog';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MultiSelectModule } from 'primeng/multiselect';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { InputMaskModule } from 'primeng/inputmask';

@NgModule({
	declarations: [
		UtilisateurModalComponent
	],
	imports: [
		UtilisateurRoutingModule,
		TableModule,
		InputTextModule,
		CommonModule,
		ButtonModule,
		DialogModule,
		RadioButtonModule,
		MultiSelectModule,
		FormsModule,
		DropdownModule,
		InputMaskModule
	],
	providers: [],
	exports: [
		UtilisateurModalComponent
	],
	bootstrap: []
})
export class UtilisateurModule {
}
