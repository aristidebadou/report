import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
	menus: MenuItem[] = [];

	ngOnInit() {
		this.menus = [
			{
				label: 'Accueil',
				icon: 'fa fa-home',
				routerLink: ['/accueil'],
				routerLinkActiveOptions: {
					exact: true
				},
			},
			{
				label: 'Utilisateur',
				icon: 'fa fa-users',
				routerLink: ['/utilisateur'],
				routerLinkActiveOptions: {
					exact: true
				},
			}
		];
	}
}
